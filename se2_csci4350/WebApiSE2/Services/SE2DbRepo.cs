﻿using System.Collections;
using WebApiSE2.DbFirstData;

namespace WebApiSE2.Services
{
    public class SE2DbRepo
    {
        private SE2_DatabaseContext _db;
        public SE2DbRepo(SE2_DatabaseContext db)
        {
            _db = db;
        }

        public IEnumerable GetBicycles()
        {
            var bicycles = _db.Bicycle;
            return bicycles;
        }
    }
}
