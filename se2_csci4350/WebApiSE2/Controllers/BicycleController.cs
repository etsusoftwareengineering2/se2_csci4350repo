﻿using Microsoft.AspNetCore.Mvc;
using WebApiSE2.DbFirstData;
using WebApiSE2.Services;

namespace WebApiSE2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BicycleController : ControllerBase
    {
        private SE2_DatabaseContext _context;
        private SE2DbRepo _repo;
        public BicycleController(SE2_DatabaseContext context, SE2DbRepo repo) 
        {
            _context = context;
            _repo = repo;
        }
         //GET api/values
        [HttpGet("/api/bicycles")]
        public JsonResult Get()
        {
            var bicycles = _repo.GetBicycles();
            return new JsonResult(bicycles);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
