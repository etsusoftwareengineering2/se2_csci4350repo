﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Component
    {
        public decimal Componentid { get; set; }
        public decimal Manufacturerid { get; set; }
        public string Productnumber { get; set; }
        public string Road { get; set; }
        public string Category { get; set; }
        public double? Length { get; set; }
        public double? Height { get; set; }
        public double? Width { get; set; }
        public double? Weight { get; set; }
        public decimal? Year { get; set; }
        public decimal? Endyear { get; set; }
        public string Description { get; set; }
        public decimal? Listprice { get; set; }
        public decimal? Estimatedcost { get; set; }
        public double? Quantityonhand { get; set; }
    }
}
