﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Bicycletubeusage
    {
        public decimal Serialnumber { get; set; }
        public decimal Tubeid { get; set; }
        public double? Quantity { get; set; }

        public virtual Bicycle SerialnumberNavigation { get; set; }
        public virtual Tubematerial Tube { get; set; }
    }
}
