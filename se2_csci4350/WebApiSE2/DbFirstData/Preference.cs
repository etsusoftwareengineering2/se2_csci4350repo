﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Preference
    {
        public string Itemname { get; set; }
        public double? Value { get; set; }
        public string Description { get; set; }
        public DateTime? Datechanged { get; set; }
    }
}
