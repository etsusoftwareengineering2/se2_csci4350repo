﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Bicycle
    {
        public Bicycle()
        {
            Bicycletubeusage = new HashSet<Bicycletubeusage>();
            Bikeparts = new HashSet<Bikeparts>();
        }

        public decimal Serialnumber { get; set; }
        public decimal Customerid { get; set; }
        public string Modeltype { get; set; }
        public decimal Paintid { get; set; }
        public double? Framesize { get; set; }
        public DateTime? Orderdate { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Shipdate { get; set; }
        public decimal? Shipemployee { get; set; }
        public decimal? Frameassembler { get; set; }
        public decimal? Painter { get; set; }
        public string Construction { get; set; }
        public decimal? Waterbottlebrazeons { get; set; }
        public string Customname { get; set; }
        public string Letterstyleid { get; set; }
        public decimal Storeid { get; set; }
        public decimal Employeeid { get; set; }
        public double? Toptube { get; set; }
        public double? Chainstay { get; set; }
        public double? Headtubeangle { get; set; }
        public double? Seattubeangle { get; set; }
        public decimal? Listprice { get; set; }
        public decimal? Saleprice { get; set; }
        public decimal? Salestax { get; set; }
        public string Salestate { get; set; }
        public decimal? Shipprice { get; set; }
        public decimal? Frameprice { get; set; }
        public decimal? Componentlist { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Letterstyle Letterstyle { get; set; }
        public virtual Modeltype ModeltypeNavigation { get; set; }
        public virtual Paint Paint { get; set; }
        public virtual Retailstore Store { get; set; }
        public virtual ICollection<Bicycletubeusage> Bicycletubeusage { get; set; }
        public virtual ICollection<Bikeparts> Bikeparts { get; set; }
    }
}
