﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Tubematerial
    {
        public Tubematerial()
        {
            Bicycletubeusage = new HashSet<Bicycletubeusage>();
        }

        public decimal Tubeid { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public double? Diameter { get; set; }
        public double? Thickness { get; set; }
        public string Roundness { get; set; }
        public double? Weight { get; set; }
        public double? Stiffness { get; set; }
        public decimal? Listprice { get; set; }
        public string Construction { get; set; }

        public virtual ICollection<Bicycletubeusage> Bicycletubeusage { get; set; }
    }
}
