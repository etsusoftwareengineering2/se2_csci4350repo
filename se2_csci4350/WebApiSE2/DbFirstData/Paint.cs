﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Paint
    {
        public Paint()
        {
            Bicycle = new HashSet<Bicycle>();
        }

        public decimal Paintid { get; set; }
        public string Colorname { get; set; }
        public string Colorstyle { get; set; }
        public string Colorlist { get; set; }
        public DateTime? Dateintroduced { get; set; }
        public DateTime? Datediscontinued { get; set; }

        public virtual ICollection<Bicycle> Bicycle { get; set; }
    }
}
