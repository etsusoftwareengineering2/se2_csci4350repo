﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Revisionhistory
    {
        public decimal Id { get; set; }
        public string Version { get; set; }
        public DateTime? Changedate { get; set; }
        public string Name { get; set; }
        public string Revisioncomments { get; set; }
    }
}
