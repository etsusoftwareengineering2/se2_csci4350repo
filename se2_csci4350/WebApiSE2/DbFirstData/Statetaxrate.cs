﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Statetaxrate
    {
        public string State { get; set; }
        public double? Taxrate { get; set; }
    }
}
