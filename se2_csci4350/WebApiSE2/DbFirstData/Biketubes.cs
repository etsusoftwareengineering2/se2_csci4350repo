﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Biketubes
    {
        public decimal Serialnumber { get; set; }
        public string Tubename { get; set; }
        public decimal Tubeid { get; set; }
        public double? Length { get; set; }
    }
}
