﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Componentname
    {
        public string Componentname1 { get; set; }
        public decimal? Assemblyorder { get; set; }
        public string Description { get; set; }
    }
}
