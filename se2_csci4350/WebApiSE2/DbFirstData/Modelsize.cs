﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Modelsize
    {
        public string Modeltype { get; set; }
        public double Msize { get; set; }
        public double? Toptube { get; set; }
        public double? Chainstay { get; set; }
        public double? Totallength { get; set; }
        public double? Groundclearance { get; set; }
        public double? Headtubeangle { get; set; }
        public double? Seattubeangle { get; set; }
    }
}
