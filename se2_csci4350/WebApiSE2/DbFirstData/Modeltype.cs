﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class Modeltype
    {
        public Modeltype()
        {
            Bicycle = new HashSet<Bicycle>();
        }

        public string Modeltype1 { get; set; }
        public string Description { get; set; }
        public decimal? Componentid { get; set; }

        public virtual ICollection<Bicycle> Bicycle { get; set; }
    }
}
