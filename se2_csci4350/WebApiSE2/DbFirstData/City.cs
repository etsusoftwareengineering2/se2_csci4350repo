﻿using System;
using System.Collections.Generic;

namespace WebApiSE2.DbFirstData
{
    public partial class City
    {
        public decimal Cityid { get; set; }
        public string Zipcode { get; set; }
        public string City1 { get; set; }
        public string State { get; set; }
        public string Areacode { get; set; }
        public decimal? Population1990 { get; set; }
        public decimal? Population1980 { get; set; }
        public string Country { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Populationcdf { get; set; }
    }
}
